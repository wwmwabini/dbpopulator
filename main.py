from flask import Flask, render_template, url_for, abort
import lorem, pymysql, random,os
from essential_generators import DocumentGenerator, MarkovWordGenerator, MarkovTextGenerator

app = Flask(__name__)

app.secret_key = os.environ['APP_SECRET_KEY']

application = app

DB_HOST = os.environ['DB_HOST']
DB_PORT = int(os.environ['DB_PORT'])
DB_USER = os.environ['DB_USER']
DB_PASS = os.environ['DB_PASS']
DB_NAME = os.environ['DB_NAME']

AUTHOR_FOR_RANGE = int(os.environ['AUTHOR_FOR_RANGE'])
POST_FOR_RANGE = int(os.environ['POST_FOR_RANGE'])
POST_PARAGRAPHS = int(os.environ['POST_PARAGRAPHS'])

SITE_TITLE = os.environ['SITE_TITLE']

gen1 = DocumentGenerator()
gen2 = DocumentGenerator(text_generator=MarkovTextGenerator(), word_generator=MarkovWordGenerator())

def opendb():
    con = pymysql.connect(host=DB_HOST, port=DB_PORT, user=DB_USER, password=DB_PASS, db=DB_NAME)
    return con

def createtables():

    con = opendb()
    cur = con.cursor()

    xo_posts_sql = """CREATE TABLE IF NOT EXISTS `xo_posts` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `post_author` varchar(256) DEFAULT NULL,
                     `post_title` text NOT NULL,
                     `post_content` text DEFAULT NULL,
                     `post_status` varchar(256) NOT NULL DEFAULT 'published',
                     `post_date` timestamp NOT NULL DEFAULT current_timestamp(),
                     PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4
                    """
    xo_authors_sql ="""CREATE TABLE IF NOT EXISTS `xo_authors` (
                     `id` int(11) NOT NULL AUTO_INCREMENT,
                     `name` varchar(256) NOT NULL,
                     `email` varchar(256) NOT NULL,
                     `url` text NOT NULL,
                     PRIMARY KEY (`id`)
                    ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8mb4"""

    cur.execute(xo_authors_sql)
    cur.execute(xo_posts_sql)

    cur.close()
    con.close()

    return 'Tables OK!'

createtables()


#author
def generate_authors():
    AUTHOR_NAME = gen1.word()
    AUTHOR_EMAIL = gen1.email()
    AUTHOR_URL = gen1.url()

    con = opendb()

    with con.cursor() as cur:
        cur.execute('INSERT INTO xo_authors (name, email, url) '
                    'VALUES(%s, %s, %s)', (AUTHOR_NAME, AUTHOR_EMAIL, AUTHOR_URL))
        con.commit()
        cur.close()
        con.close()


    return 'Author OK'

#posts
def generate_posts():
    con = opendb()
    cur = con.cursor()

    cur.execute("SELECT id FROM xo_authors;")
    authors_list = list(cur.fetchall())

    random_author = random.choice(authors_list)

    POST_AUTHOR = random_author[0]
    POST_TITLE = gen2.sentence()
    POST_CONTENT = ''

    for i in range(POST_PARAGRAPHS):
        POST_CONTENT = POST_CONTENT + gen2.paragraph()


    cur.execute('INSERT INTO xo_posts (post_author, post_title, post_content) '
                'VALUES(%s, %s, %s)', (POST_AUTHOR, POST_TITLE, POST_CONTENT))
    con.commit()
    cur.close()
    con.close()

    return 'Post OK'

@app.route('/')
def home():
    con = opendb()
    cur = con.cursor()

    try:
        #cur.execute('SELECT id FROM xo_posts ORDER BY id DESC limit 1') #gives wrong result incase entries fail
        cur.execute('SELECT COUNT(*) FROM xo_posts;')
        (NO_OF_ARTICLES, *others) = cur.fetchone()

        #cur.execute('SELECT id FROM xo_authors ORDER BY id DESC limit 1') #gives wrong result
        cur.execute('SELECT COUNT(*) FROM xo_authors;')
        (NO_OF_AUTHORS, *others) = cur.fetchone()

        cur.close()
        con.close()
    except Exception as e:
        return render_template('500.html'), 500

    return render_template('index.html', site_title=SITE_TITLE, no_of_articles=NO_OF_ARTICLES, no_of_authors=NO_OF_AUTHORS)


@app.route('/module/generate/1')
def generate():
    for i in range(AUTHOR_FOR_RANGE):
        generate_authors()

    for j in range(POST_FOR_RANGE):
        generate_posts()
    return 'OK'

@app.route('/posts/<id>')
def posts(id):
    con = opendb()
    cur = con.cursor()
    post_id = int(id) - 1

    try:
        cur.execute('SELECT post_author, post_title, post_content FROM xo_posts ORDER BY id LIMIT %s,1', (post_id,))
        posts_result = list(cur.fetchone())

        print(posts_result)

        post_author_id = posts_result[0]
        post_title = posts_result[1]
        post_content = posts_result[2]

        cur.execute('SELECT email FROM xo_authors WHERE id= %s', (post_author_id,))

        author_result = list(cur.fetchone())
        post_author = author_result[0]

        POST_TITLE = post_title
        POST_CONTENT = post_content
        POST_AUTHOR = post_author

        cur.close()
        con.close()

    except Exception as e:
        return render_template('404.html'), 404

    return render_template('generate.html', site_title=SITE_TITLE, post_title=POST_TITLE, post_content=POST_CONTENT, post_author=POST_AUTHOR)

@app.errorhandler(404)
def pagenotfound(e):
    return render_template('404.html'), 404

@app.errorhandler(500)
def internalservererror(e):
    return render_template('500.html'), 500

if __name__ == "__main__":
    app.run(debug=True, port=5009)