**Introduction**

MySQL DB Populator is a simple flask application that generates fake contents and authors and saves them in a MySQL database. The purpose of this application is to be able to create huge database on MySQL that will allow you to run tests on your database infrastructure without using live data. Bt tweaking a few attributes, you can create a database as big as 100GB in a few minutes.

The application runs using MySQL/MariaDB but can be changed a bit so as to use with other databases such as PostgreSQL or MSSQL

**How It Works**

The application generates posts and authors as per set variables, when the URI /module/generate/1 is accessed. Once you deploy the files, it is necessary to set a cron job with a curl command to the URL http://domain.tld/module/generate/1 so that the process of post/author generation can run occassionally.

Once the data is pushed into your database, you can test access to various posts by using the URI /posts/<number> where <number> is the post id starting from 1 to the maximum available. You can get the maximum posts available by checking the home page of the application once deployed. This number is displayed there

**Installation**

You can run the application on any envrironment that has python and MySQL but this particular files as they are, are ment to run as a container, connecting to a an external database [i.e, database is not part of the container]

To get started, simply pull the docker image from docker hub, https://hub.docker.com/r/wwmwabini/dbpopulator and set the environment variables as shown in the Environment Variables section below

You can also customise the application so you can deploy it on baremetal or cPanel hosting without containers.

PS: Remeber to set a cron job to curl into /module/generate/1 so that more aricles are auto generated as time goes by. eg 
*/10 * * * * curl http://domain.tld/module/generate/1 to generate articles every 10 minutes.

**Envrionment Variables**

You need to set the following envrionment variables

APP_SECRET_KEY=xxxx
DB_HOST=localhost
DB_USER=root
DB_PASS=xxxx
DB_PORT=3306
DB_NAME=dbname
AUTHOR_FOR_RANGE=10
POST_FOR_RANGE=15
POST_PARAGRAPHS=1
SITE_TITLE='MySQL Database Populator'

**Envrionment Variables Explained**

APP_SECRET_KEY - This is your flask secret key
DB_HOST - This is the database secret key
DB_USER - This is the database username
DB_PASS - This is the database password
DB_PORT - This is the database port
DB_NAME - This is the database name
AUTHOR_FOR_RANGE - This is the number of authors you want generated everytime a cronjob is run.
POST_FOR_RANGE - This is the number of posts you want generated everytime a cronjob is run. 
POST_PARAGRAPHS - This is the number of paragraphs you want to have per post saved into the database.
SITE_TITLE - This is the words you want to display as your site title. Default words are 'MySQL Database Populator'

**How to control amount of content generated**

The number of posts and authors generated can be altered using diffrent variables so as to grow the database as fast as you would like. The following can be envrionment variables can be altered to achieve faster article generation

- AUTHOR_FOR_RANGE - increasing this value will generate more authors with each cron run. Decreasing it has the opposite effect.

- POST_FOR_RANGE -  increasing this value will generate more posts with each cron run. Decreasing it has the opposite effect. This is one of the recommended values to alter so as to get bigger databases since number of posts will be more.

- POST_PARAGRAPHS - increasig this value will generate bigger posts, hence also increasing the size of the database. e.g if set to 10, each post will have 10 paragraphs, which would be bigger in size to one that had 5 paragraphs instead. This is another of the recommended values to alter so as to get bigger databases since each post has a lot of data.

- The cronjob - If you run the cron job more frequirently, data is generated more frequently and hence your database becomes bigger, faster.


