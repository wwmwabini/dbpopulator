FROM alpine:3.13
MAINTAINER Wallace Mwabini "wallace@rawle.systems"
RUN apk add --no-cache python3 py3-pip
COPY ./requirements.txt /requirements.txt
ENV FLASK_APP main.py
WORKDIR /
RUN pip3 install -r requirements.txt
COPY . /
EXPOSE 5009
CMD [ "python3", "-m" , "flask", "run", "--host=0.0.0.0", "--port=5009"]